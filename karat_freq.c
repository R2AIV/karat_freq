#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>

/*
 *
 * Frequency channel ROM HEX file maker for Karat-M by UD0CAJ
 * Written 140122 by R2AIV
 * Rework  010222 by R2AIV
 * 
 */

int main(int argc, char *argv[])
{
	uint32_t FreqArray[20];
	uint8_t TotalFreqs = 0;
	FILE *HexOutput;
	uint16_t MaxAddr;

	printf("Frequency channel EEPROM HEX file generator v0.1\r\n");
	printf("R2AIV for Karat-M by UD0CAJ (c) 140122\r\n");


	HexOutput = fopen("freqdata.hex","w");

	if(argc <= 1)
	{
		printf("USAGE: karat_freq [up to 16 frequencies]\r\n");
		return 0;
	}
	else if(argc > 17)
	{
		printf("Entered more than 16 frequencies!\r\n");
		return 0;
	}

	TotalFreqs = argc - 1;

	printf("Entered %d frequenci(es)!\r\n",TotalFreqs);

	// Preparing array of frequencies
	memset((void *)&FreqArray,0,sizeof(FreqArray));

	MaxAddr = (TotalFreqs - 1) * 4;

	for(int i=0;i<TotalFreqs;i++)
	{
		uint32_t crc_seed = 0;
		uint8_t crc;
		uint8_t FreqArrayBytes[4];
		uint16_t MaxAddr = 0;

		memset((void *)&FreqArrayBytes,0,sizeof(FreqArrayBytes));

		sscanf(argv[i+1],"%d",&FreqArray[i]);
		FreqArray[i] *= 1000;										// kHz to Hz

		crc_seed += 4; 												// Record length
		crc_seed += (uint8_t)((i*4) & 0xFF00) >> 8;					// Offset (high)
		crc_seed += (i*4) & 0x00FF;									// Offset (low)
		crc_seed += 0;												// RecType
		crc_seed += (uint8_t)((FreqArray[i] & 0xFF000000) >> 24);	// Data (4 byte)
		crc_seed += (uint8_t)((FreqArray[i] & 0x00FF0000) >> 16);
		crc_seed += (uint8_t)((FreqArray[i] & 0x0000FF00) >> 8);
		crc_seed += (uint8_t)((FreqArray[i] & 0x000000FF) >> 0);
		crc = (uint8_t)(0x0100 - crc_seed);

		FreqArrayBytes[0] = (FreqArray[i] & 0x000000FF) >> 0;		// LSB
		FreqArrayBytes[1] = (FreqArray[i] & 0x0000FF00) >> 8;
		FreqArrayBytes[2] = (FreqArray[i] & 0x00FF0000) >> 16;
		FreqArrayBytes[3] = (FreqArray[i] & 0xFF000000) >> 24;		// MSB

		fprintf(HexOutput,"%s%.2X%.4X%.2X%.2X%.2X%.2X%.2X%.2X\n",
			":",
			4, 
			i*4,
			0, 
			FreqArrayBytes[0],
			FreqArrayBytes[1],
			FreqArrayBytes[2],
			FreqArrayBytes[3],
			crc);
	};

	for(uint16_t j = MaxAddr + 4; j < 64; j += 4)
	{
		fprintf(HexOutput, ":04%.4X00FFFFFFFF%.2X\n",j, (uint8_t)(0x100 - (uint8_t)(0x04 + j + 0x3FC)));
	};

	// Writing tail of HEX file
	
	fprintf(HexOutput,":00000001FF");	

	fclose(HexOutput);

	printf("That's all, folks!(c)\r\n");

	return 0;
};

